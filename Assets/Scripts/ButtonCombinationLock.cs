using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonCombinationLock : MonoBehaviour
{
    [SerializeField] private Text _buttonText;
    [SerializeField] private Text _codeText;

    [SerializeField] private CombinationLock _combinationLock;

    public void AddText()
    {
        _codeText.text += _buttonText.text;
    }

    public void DeleteText()
    {
        int i = _codeText.text.Length;
        if(i!=0)
            _codeText.text = $"{_codeText.text.Remove(i-1)}";

    }

    public void Enter()
    {
        _combinationLock.CheckCode();
    }


}
