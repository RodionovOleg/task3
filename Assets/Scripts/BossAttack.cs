using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossAttack : MonoBehaviour
{
   
    private void OnTriggerEnter(Collider other)
    {
  
        if (other.TryGetComponent(out Game.Player player))
        {
            player.Kill();
        }
        else if(other.TryGetComponent(out ShieldFunctions shieldFunctions))
            shieldFunctions.Disable();

    }
}
