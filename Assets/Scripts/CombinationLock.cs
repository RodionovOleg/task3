using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CombinationLock : MonoBehaviour
{

    [SerializeField] private Text _EnterText;
    [SerializeField] private Text _BackspaceText;

    [SerializeField] private Text _codeText;
    [SerializeField] private Image _indicator;
    [SerializeField] private GameObject _door;
    [SerializeField] private Door _doorCode;

    private int code;
    private int codeGenerated;
    private void Start()
    {
        _EnterText.text = "\u21B5";
        _BackspaceText.text = "\u2190";

    }

    public void CheckCode()
    {
        code = int.Parse(_codeText.text);
        codeGenerated = _doorCode.GetCode();

        if (code == codeGenerated)
        {
            _indicator.color = Color.green;
            Destroy(_door);
        }
    }
 

}
