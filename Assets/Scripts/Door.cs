using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Door : MonoBehaviour
{
    [SerializeField] private Transform _doorPoint;
    [SerializeField] private Game.Player _player;

    [SerializeField] private GameObject helper;
    [SerializeField] private Text _helpText;
    [SerializeField] private GameObject helperDoor;
    [SerializeField] private Text _helpTextDoor;

    [SerializeField] private GameObject _combinationLock;
    [SerializeField] private Text _noteText;

    private bool actionDoor = false;
    private int codeGenerated;

    private float _timer = 3;
    private bool _timerCheck = true;

    private void Start()
    {
        codeGenerated = Random.Range(99, 1000);
        _noteText.text= $"{codeGenerated}";

        if (SceneManager.GetActiveScene().buildIndex == 5)
        {
            helper.SetActive(true);
            _helpText.text = "����� ��� � ����� � ����� �� �������";
        }

    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.F) && actionDoor)
            CombinationLock();

        if (_player.IsAlive)
            checkDoor();

        if (SceneManager.GetActiveScene().buildIndex == 5 && _timerCheck)
            checkHelper();

    }


    private void checkDoor()
    {
        var flatDoorPosition = new Vector2(_doorPoint.transform.position.x, _doorPoint.transform.position.z);
        var flatPlayerPosition = new Vector2(_player.transform.position.x, _player.transform.position.z);

        if (flatDoorPosition == flatPlayerPosition)
        {
            if (SceneManager.GetActiveScene().buildIndex == 5)
            {
                helperDoor.SetActive(true);
                _helpTextDoor.text = "��� �������� ����� ����� F";
            }

            actionDoor = true;
        }
        else
        {
            if (SceneManager.GetActiveScene().buildIndex == 5)
                helperDoor.SetActive(false);


            actionDoor = false;
        }
    }

    private void CombinationLock()
    {
        _combinationLock.SetActive(true);
    }

    public int GetCode()
    {
        return codeGenerated;
    }


    private void checkHelper()
    {
        if (_timer <= 0)
        {
            helper.SetActive(false);
            _timerCheck = false;
        }
        else
            _timer -= Time.deltaTime;
    }

}
