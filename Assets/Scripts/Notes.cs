using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Notes : MonoBehaviour
{
    [SerializeField] private Transform[] _notesPoints;
    [SerializeField] private Game.Player _player;
    [SerializeField] private GameObject _note;

    private int _notesNumber;



    private void Start()
    {
        _notesNumber = Random.Range(0, _notesPoints.Length);
    }

    private void Update()
    {
        if (_player.IsAlive)
            CheckNotes();
    }

    private void CheckNotes()
    {
        var flatNotesAPosition = new Vector2(_notesPoints[_notesNumber].transform.position.x, _notesPoints[_notesNumber].transform.position.z);
        var flatPlayerPosition = new Vector2(_player.transform.position.x, _player.transform.position.z);

        if(flatPlayerPosition == flatNotesAPosition)
        {
            _note.SetActive(true);
        }
        else
        {
            _note.SetActive(false);
        }

    }


}
