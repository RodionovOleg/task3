using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Note : MonoBehaviour
{
    [SerializeField] private Game.Player _player;

    [SerializeField] private GameObject _note;
    [SerializeField] private Notes _notes;

    private void Update()
    {
        if (_player.IsAlive)
            CheckNote();
    }

    private void CheckNote()
    {
        var flatNoteAPosition = new Vector2(transform.position.x, transform.position.z);
        var flatPlayerPosition = new Vector2(_player.transform.position.x, _player.transform.position.z);


        if (flatNoteAPosition == flatPlayerPosition)
        {
            if (Input.GetKeyDown(KeyCode.F))
            {
                _note.SetActive(true);

            }
        }
    }
}
