using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Turret : MonoBehaviour
{
    [SerializeField] private float _timeBtwShots;
    [SerializeField] private float _startTimeBtwShots;

    [SerializeField] private GameObject bullet;
    [SerializeField] private LayerMask _playerMask;
    [SerializeField] private LayerMask _playerWall;

    [SerializeField] private Game.Player _player;

    [SerializeField] private Text _helpText;
    [SerializeField] private GameObject helper;

    private float _timer = 3;
    private bool _timerCheck = true;

    private void Start()
    {
        _timeBtwShots = 0;

        if (SceneManager.GetActiveScene().buildIndex == 3)
        {
            helper.SetActive(true);
            _helpText.text = "׸���� ���� - ��� ������\n ���� ���������!";
        }
    }

    private void Update()
    {
        if (_player.IsAlive)
            Shot();

        if (SceneManager.GetActiveScene().buildIndex == 3 && _timerCheck)
            checkHelper();

    }

    private void Shot()
    {
        Ray ray1 = new Ray(transform.position, transform.forward);
        Ray ray2 = new Ray(transform.position, -transform.forward);
        Ray ray3 = new Ray(transform.position, transform.right);
        Ray ray4 = new Ray(transform.position, -transform.right);

        bool checkPlayer1 = Physics.Raycast(ray1, out RaycastHit hit1, 10f, _playerMask);
        bool checkPlayer2 = Physics.Raycast(ray2, out RaycastHit hit2, 10f, _playerMask);
        bool checkPlayer3 = Physics.Raycast(ray3, out RaycastHit hit3, 10f, _playerMask);
        bool checkPlayer4 = Physics.Raycast(ray4, out RaycastHit hit4, 10f, _playerMask);

        bool checkWall1 = Physics.Raycast(ray1, out RaycastHit hit5, 10f, _playerWall);
        bool checkWall2 = Physics.Raycast(ray2, out RaycastHit hit6, 10f, _playerWall);
        bool checkWall3 = Physics.Raycast(ray3, out RaycastHit hit7, 10f, _playerWall);
        bool checkWall4 = Physics.Raycast(ray4, out RaycastHit hit8, 10f, _playerWall);


        if (checkPlayer1 && hit1.distance < hit5.distance)
            BulletGeneration();
        if (checkPlayer2 && hit2.distance < hit6.distance)
            BulletGeneration();
        if (checkPlayer3 && hit3.distance < hit7.distance)
            BulletGeneration();
        if (checkPlayer4 && hit4.distance < hit8.distance)
            BulletGeneration();
    }

    private void BulletGeneration()
    {
        if (_timeBtwShots <= 0)
        {
            Instantiate(bullet, transform.position, Quaternion.identity);
            _timeBtwShots = _startTimeBtwShots;
        }
        else
        {
            _timeBtwShots -= Time.deltaTime;
        }
    }


    private void checkHelper()
    {
        if (_timer <= 0)
        {
            helper.SetActive(false);
            _timerCheck = false;
        }
        else
            _timer -= Time.deltaTime;
    }
}
