using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Shild : MonoBehaviour
{

    [SerializeField] private GameObject _shild;
    [SerializeField] private Material _shildMaterial;
    [SerializeField] private Material _playerMaterial;
    [SerializeField] private Game.Player _player;

    [SerializeField] private Text _helpText;
    [SerializeField] private GameObject helper;

    private MeshRenderer _renderer;

    private float _timer = 5;
    private bool _timerCheck = true;



    private void Awake()
    {
        _renderer = _player.GetComponent<MeshRenderer>();
    }
    private void Start()
    {
        if (SceneManager.GetActiveScene().buildIndex == 4) {
            helper.SetActive(true);
            _helpText.text = "E - �������� ���\nQ - ��������� ���\n��� ��������� ������� ��� �����������, �� ��� ����� �������� �������";
        }

    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.E))
            Enable();
        if (Input.GetKeyDown(KeyCode.Q))
            Disable();

        if (SceneManager.GetActiveScene().buildIndex == 4 && _timerCheck)
            checkHelper();
    }

    public void Enable()
    {
        _shild.SetActive(true);
        _renderer.sharedMaterial = _shildMaterial;
    }

    public void Disable()
    {
        _shild.SetActive(false);
        _renderer.sharedMaterial = _playerMaterial;
    }

    private void checkHelper()
    {
        if (_timer <= 0)
        {
            helper.SetActive(false);
            _timerCheck = false;
        }
        else
            _timer -= Time.deltaTime;
    }

}
