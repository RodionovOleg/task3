using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Boss : MonoBehaviour
{
    [SerializeField] private float _timeBtwShots;
    [SerializeField] private float _startTimeBtwShots;

    [SerializeField] private Game.Player _player;
    [SerializeField] private LayerMask _playerMask;

    private Vector3 _attackMoving;
    [SerializeField] private Transform[] _rayPoints;

    [SerializeField] private GameObject[] _attack;
    private int i;


    [SerializeField] private GameObject helper;
    [SerializeField] private Text _helpText;
    private float _timer = 3;
    private bool _timerCheck = true;

    private void Start()
    {
        _timeBtwShots = 0.5f;
        i = 0;

        if (SceneManager.GetActiveScene().buildIndex == 6)
        {
            helper.SetActive(true);
            _helpText.text = "���� ����";
        }
    }

    private void Update()
    {
        if (_player.IsAlive)
            Shot();

        if (SceneManager.GetActiveScene().buildIndex == 6 && _timerCheck)
            checkHelper();
    }


    private void Shot()
    {
        Ray ray1 = new Ray(transform.position, transform.forward);
        Ray ray2 = new Ray(_rayPoints[0].transform.position, transform.forward);
        Ray ray3 = new Ray(_rayPoints[1].transform.position, transform.forward);
        Ray ray4 = new Ray(_rayPoints[2].transform.position, transform.forward);
        Ray ray5 = new Ray(_rayPoints[3].transform.position, transform.forward);

        bool checkPlayer1 = Physics.Raycast(ray1, out RaycastHit hit1, 14f, _playerMask);
        bool checkPlayer2 = Physics.Raycast(ray2, out RaycastHit hit2, 14f, _playerMask);
        bool checkPlayer3 = Physics.Raycast(ray3, out RaycastHit hit3, 14f, _playerMask);
        bool checkPlayer4 = Physics.Raycast(ray4, out RaycastHit hit4, 14f, _playerMask);
        bool checkPlayer5 = Physics.Raycast(ray5, out RaycastHit hit5, 14f, _playerMask);

        if (checkPlayer1 || checkPlayer2 || checkPlayer3 || checkPlayer4 || checkPlayer5)
            BossAttack();
        else if(i > 0)
        {
            _attack[i - 1].SetActive(false);
            i = 0;
        }else if(i == 0)
            _attack[i].SetActive(false);

    }

    private void BossAttack()
    {
        if (_timeBtwShots <= 0)
        {
            if (i == _attack.Length)
            {
                _attack[i-1].SetActive(false);
                i = 0;
            }

            if (i > 0)
                _attack[i-1].SetActive(false);

            _attack[i].SetActive(true);
            i++;

            _timeBtwShots = _startTimeBtwShots;
        }
        else
        {
            _timeBtwShots -= Time.deltaTime;
        }
    }


    private void checkHelper()
    {
        if (_timer <= 0)
        {
            helper.SetActive(false);
            _timerCheck = false;
        }
        else
            _timer -= Time.deltaTime;
    }
}
