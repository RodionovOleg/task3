using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    [SerializeField] private float _speed;

    private Transform _player;
    private Vector3 _target;

    private void Start()
    {
        _player = GameObject.FindWithTag("Player").transform;
        _target = _player.transform.position;
    }

    private void Update()
    {
        transform.position = Vector3.MoveTowards(transform.position, _target, _speed*Time.deltaTime);

        if (transform.position == _target) Destroy(gameObject);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.TryGetComponent(out ShieldFunctions shieldFunctions))
        {
            Destroy(gameObject);
            shieldFunctions.Disable();
        }
        else if (other.TryGetComponent(out Game.Player player))
        {
            player.Kill();
        }

            
           

    }

}
