using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CloseCombinationLock : MonoBehaviour
{
    [SerializeField] private GameObject _combinationLock;
    public void Close()
    {
        _combinationLock.SetActive(false);
    }
    

}
