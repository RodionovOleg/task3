using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Portal : MonoBehaviour
{
    [SerializeField] private Transform[] _portalsPoints;
    [SerializeField] private Game.Player _player;

    [SerializeField] private GameObject helper;
    [SerializeField] private Text _helpText;

    private int portalIndex = 0;
    private bool actionPortal = false;

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.F))
            actionPortal = true;
     
        if(_player.IsAlive)
            CheckPortal();

    }


    private void Moving()
    {
        if (actionPortal == true)
        {
            portalIndex++;
            if (portalIndex >= _portalsPoints.Length) portalIndex = 0;

            var flatPortalPosition = new Vector3(_portalsPoints[portalIndex].transform.position.x, _player.transform.position.y, _portalsPoints[portalIndex].transform.position.z);
            _player.transform.position = flatPortalPosition;
            actionPortal = false;
        }
    }

    private void CheckPortal()
    {
        var flatPortalAPosition = new Vector2(_portalsPoints[0].transform.position.x, _portalsPoints[0].transform.position.z);
        var flatPortalBPosition = new Vector2(_portalsPoints[1].transform.position.x, _portalsPoints[1].transform.position.z);
        var flatPlayerPosition = new Vector2(_player.transform.position.x, _player.transform.position.z);

        if (flatPortalAPosition == flatPlayerPosition || flatPortalBPosition == flatPlayerPosition)
        {
            if (SceneManager.GetActiveScene().buildIndex == 2)
            {
                helper.SetActive(true);
                _helpText.text = "��� ��������� ������� ����� F";
            }
            Moving();
        }
        else if(SceneManager.GetActiveScene().buildIndex == 2)
            helper.SetActive(false);
    }

}
